package com.reve.iprouter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class SoCatThread extends Thread{
    static int failCount = 0;
    static int idGenerator = 0;
    int id;
    int timeoutInMillis = 10000;
    int debugMode = 0;
    long socatPid;

    boolean running;
    Process processCommandRun;
    ProcessHandle processSocat = null;
    ProcessHandle processSocatParent = null;
    ProcessHandle processSocatListen = null;
    ProcessHandle processSocatForward = null;

    long previousForwardPid = 0;

    String command;

    int state = 1;

    String serverPort1;
    String serverPort2;

    public ByteArrayOutputStream result = new ByteArrayOutputStream();
    public byte[] buffer = new byte[1024];

    public SoCatThread(String[] args, int debug) {
        debugMode = debug;
        id = ++idGenerator;

        String serverIp = args[0];
        serverPort1 = args[1];
        serverPort2 = args[2];
        String termIp = args[3];
        String termPort = args[4];
        int timeout = Integer.parseInt(args[5]);

        command = CoreSoCatOperation(serverIp, serverPort1, serverPort2, termIp, termPort, timeout)+"&";
        if(debugMode == 1){
            System.out.println(id+" built command => "+command);
        }
    }

    /**
     * Its by default a bi-directional connection so no mirror rule addition is needed.
     * it will listen on server ip port1 and forward to term Ip:port using port2 and vice-versa.
     * ruleMode is also not needed as it just needs deletation of PID's running on corrsponding port to
     * destroy corresponding socat rules
     * @param serverIp server ip
     * @param serverPort1 server port 1
     * @param serverPort2 server port 2
     * @param termIp routing ip
     * @param termPort routing port
     * @param timeout timeout
     * @return command
     */
    private String CoreSoCatOperation(String serverIp, String serverPort1, String serverPort2,
                                     String termIp, String termPort, int timeout){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("sudo SOCAT_SOCKADDR=").append(serverIp).append(" socat -d -d -T ").append(timeout)
                .append(" UDP4-LISTEN:").append(serverPort1)
                .append(",reuseaddr,fork UDP4:").append(termIp).append(":").append(termPort).append(",")
                .append("bind=").append(serverIp).append(":").append(serverPort2);
        return stringBuilder.toString();
    }

    public Process runCommandFromThread(String command) {
        Process process = null;
        try {
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("sh", "-c", command);
            process = processBuilder.start();
            int exitCode = process.waitFor();
            if(exitCode != 0){
                if(debugMode == 1) System.out.println(id+" couldn't start properly");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return process;
    }

    public void run() {
        // sudo java -jar SoCatImpl.jar 192.168.11.131 132 133 192.168.11.130 130 10 2 1
        // sudo /usr/jdk-9.0.1/bin/java -jar SoCatImpl.jar 192.168.11.131 132 133 192.168.11.130 130 10 2 1
        running = true;
        processCommandRun = runCommandFromThread(command);

        try {
            if(debugMode == 1) System.out.println(id+" command run pid : " + processCommandRun.pid());

            if(debugMode == 1) System.out.println(id+" currentPid(native):"+ProcessHandle.current().pid());
            processSocatListen = processHandlerOfPid(pidOnPort(Integer.parseInt(serverPort1)));
            if(debugMode == 1) System.out.println(id+" processSocatListen : (pid)" + processSocatListen.pid()
                    + " (isAlive)" + processSocatListen.isAlive());
            processSocat = processSocatListen.parent().get();
            socatPid = processSocat.pid();
            if(debugMode == 1) System.out.println(id+" socat pid : " + socatPid);
            processSocatParent = processSocat.parent().get();
            if(debugMode == 1) System.out.println(id+" processSocatParent : (pid)"+processSocatParent.pid());

            processCommandRun.destroy();
            if (processCommandRun.isAlive()) processCommandRun.destroyForcibly();
            if(debugMode == 1) System.out.println(id+" running started ..");
            if(debugMode == 1) System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if(debugMode == 1) System.out.println(id+" problem in thread config loading, adding 500ms sleep ..");
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        while (running) {
            if(debugMode == 1) System.out.println(id+" "+System.currentTimeMillis());
            try {
                Thread.sleep(timeoutInMillis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(debugMode == 1){
                System.out.println(id+" "+System.currentTimeMillis());
                System.out.println(id+" processListen : (pid)" + processSocatListen.pid()
                        + " (isAlive)" + processSocatListen.isAlive());
                System.out.println(id+" processSocatListen child count : " + processSocatListen.children().count());
            }

            if (processSocatListen.children().count() > 0) {
                processSocatForward = processSocatListen.children().findFirst().get();
                if(debugMode == 1) System.out.println(id+" processForward : (pid)" + processSocatForward.pid() + " (isAlive)" + processSocatForward.isAlive());
            }else{
                if(debugMode == 1) System.out.println(id+" no processForward pid");
            }

            stateTimeoutCheck(); //timeout check
            if(processSocatListen.children().count()>0){
                if(debugMode == 1) System.out.println(id+" updating prev process forward pid "+processSocatListen.children().findFirst().get().pid());
                previousForwardPid = processSocatListen.children().findFirst().get().pid();
            }
            if(debugMode == 1) System.out.println();
        }

    }

    public String runCommandReturnString(String command) throws IOException, InterruptedException {
        result.reset();
        Arrays.fill(buffer, (byte)0);
        Process proc = Runtime.getRuntime().exec(command);
        for (int length; (length = proc.getInputStream().read(buffer)) != -1; ) {
            result.write(buffer, 0, length);
        }
        int exitCode = proc.waitFor();
        if(exitCode != 0){
            if(debugMode == 1) System.out.println(id+" exit code "+exitCode+" on command : "+command);
        }
        // StandardCharsets.UTF_8.name() > JDK 7
        return result.toString("UTF-8");
    }

    private long pidOnPort(int port) throws IOException, InterruptedException {

        int retryCount = 0;
        String pid = null;
        try {
            pid = runCommandReturnString("sudo lsof -t -i:" + port);
            if (pid.length() == 0){
                if(debugMode == 1) System.out.println(id+" problem on detecting pid on port "+port);
            }
            pid = pid.split("\n")[0];
            return Long.parseLong(pid);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            if (debugMode == 1) System.out.println(id + " problem in parsing pid : " + pid + " on port : " + port+" returning -1");
            //e.printStackTrace();
        }
        return -1;
    }

    private long parentPid(long pid){
        return ProcessHandle.of(pid).get().parent().get().pid();
    }

    private ProcessHandle processHandlerOfPid(long pid){
        return ProcessHandle.of(pid).get();
    }

    private boolean stateTimeoutCheck(){
        if(debugMode == 1) System.out.println(id+" stateTimeout check : {");
        boolean b = coreTimeoutCheck();
        if(b == true ){
            if(state == 1){
                state = 2;
                if(debugMode == 1) System.out.println(id+" going to timeout phase-2 }");
                return false;
            }else if(state == 2){
                if(debugMode == 1) System.out.println(id+" being killed }");
                kill();
                return true;
            }
        }else{
            if(state == 1){
                if(debugMode == 1) System.out.println(id+" connection running normal }");
                return false;
            }else if(state == 2){
                if(debugMode == 1) System.out.println(id+" falling back to timeout phase-1 }");
                state = 1;
                return false;
            }
        }
        if(debugMode == 1) System.out.println(id+" something wrong in timeoutCheck, state : "+state+" coreTimeoutCheck : "+b);
        return true;
    }

    private boolean coreTimeoutCheck(){
        if(debugMode == 1) System.out.print(id+" coreTimeoutCheck : {");
        if(processSocatListen.children().count() > 0){ // listen process running
            if(processSocatListen.children().findFirst().get().pid() == previousForwardPid || previousForwardPid == 0){
                // forward process running
                if(debugMode == 1) System.out.println(id+" no timeout (curForwardPid == prevForwardPid || previousForwardPid == 0)"
                        + " cur: "+ processSocatListen.children().findFirst().get().pid()
                        + " prev: "+ previousForwardPid + " }"
                );
                return false;
            }else{
                if(debugMode == 1) System.out.println(id+" timeout (curForwardPid = prevForwardPid)"
                        + " cur: "+ processSocatListen.children().findFirst().get().pid()
                        + " prev: "+ previousForwardPid + " }"
                );
                return true;
            }
        }else{
            if(debugMode == 1) System.out.println(id+" timeout (child count == 0)" + " }");
            return true;
        }
    }

    public void kill(){
        running = false;
        if(debugMode == 1) System.out.println();
        if(debugMode == 1) System.out.println(id+" destroying at "+System.currentTimeMillis());
        if(processSocatParent != null && processSocatParent.isAlive()){
            if(debugMode == 1 ) System.out.println(id+" destroying socatParentPid : "+ processSocatParent.pid());
            processSocatParent.destroyForcibly();
        }
        if(processSocat != null && processSocat.isAlive()){
            if(debugMode == 1 ) System.out.println(id+" destroying socatPid : "+ socatPid);
            processSocat.destroyForcibly();
        }
        if(processSocatListen != null &&processSocatListen.isAlive()){
            if(debugMode == 1 ) System.out.println(id+" destroying socatListenPid : "+ processSocatListen.pid());
            processSocatListen.destroyForcibly();
        }
        if(processSocatForward != null && processSocatForward.isAlive()){
            if(debugMode == 1 ) System.out.println(id+" destroying socatForwardPid : "+ processSocatForward.pid());
            processSocatForward.destroyForcibly();
        }

    }

    public static void main(String[] args) {
        String pid = "fuji\n";
        System.out.println(pid);
        System.out.println();
        System.out.println("{"+pid.split("\n")[0]+"}");
    }
}
