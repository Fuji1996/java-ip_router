package com.reve.iprouter;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class SoCatImpl {



    /**
     * Its by default a bi-directional connection so no mirror rule addition is needed.
     * it will listen on server ip port1 and forward to term Ip:port using port2 and vice-versa.
     * ruleMode is also not needed as it just needs deletation of PID's running on corrsponding port to
     * destroy corresponding socat rules
     * @param serverIp server ip
     * @param serverPort1 server port 1
     * @param serverPort2 server port 2
     * @param termIp routing ip
     * @param termPort routing port
     * @param timeout timeout
     * @return command
     */
    static String CoreSoCatOperation(String serverIp, String serverPort1, String serverPort2,
                                     String termIp, String termPort, int timeout){
        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("sudo SOCAT_SOCKADDR=").append(serverIp).append(" socat -d -d -T ").append(timeout)
//                .append(" UDP4-LISTEN:").append(serverPort1)
//                .append(",reuseaddr,fork UDP4:").append(termIp).append(":").append(termPort).append(",")
//                .append("bind=").append(serverIp).append(":").append(serverPort2);
        stringBuilder.append("SOCAT_SOCKADDR=").append(serverIp)
                .append(" socat -T ").append(timeout).append(" -t ").append(timeout)
                .append(" UDP4-LISTEN:").append(serverPort1)
                .append(",reuseaddr,fork UDP4:").append(termIp).append(":").append(termPort).append(",")
                .append("bind=").append(serverIp).append(":").append(serverPort2).append("&");
        return stringBuilder.toString();
    }

    @Deprecated
    public static ArrayList<String> runCommandReturnArraylist(String command, int debugMode) {
        ArrayList<String> arrayList = new ArrayList<>();

        try {
            ProcessBuilder processBuilder = new ProcessBuilder();
            
            processBuilder.command("sh", "-c", command);
            Process process = processBuilder.start();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(process.getErrorStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                arrayList.add(line);
                if(debugMode == 1) System.out.println(line);
            }
            int exitCode = process.waitFor();
            if(debugMode == 1) System.out.println("\nExited with error code : "+exitCode);
            System.out.println();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
        return arrayList;
    }

    @Deprecated
    synchronized public static Process runCommandFromThread(String command) {
        Process process = null;
        try {
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("sh", "-c", command);
            process = processBuilder.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return process;
    }

    @Deprecated
    public static ArrayList<String> runCommand(String command, int debugMode) throws IOException, InterruptedException {
        ByteArrayOutputStream rawLine = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        ArrayList<String> lines = new ArrayList<>();

        Process proc = Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", command});
//        Process proc = Runtime.getRuntime().exec(command);

        for (int length; (length = proc.getInputStream().read(buffer)) != -1; ) {
            rawLine.write(buffer, 0, length);
            if(debugMode == 1) System.out.print(rawLine.toString("UTF-8"));
            lines.add(rawLine.toString("UTF-8"));
            rawLine.reset();
        }
        proc.waitFor();
        // StandardCharsets.UTF_8.name() > JDK 7
        return lines;
    }

    public static HashMap<Integer, Long> runNetstatCommand(int parsingMode) throws IOException, InterruptedException {
        HashMap<Integer, Long> hashMap;
        ProcessBuilder processBuilder = new ProcessBuilder(new String[]{"/bin/bash", "-c", "sudo netstat -ulpn | grep 'socat'"});
        Process process = processBuilder.start();
        long t1 = System.currentTimeMillis();
        if(parsingMode == 0){
            hashMap = netstatParser1(process.getInputStream());
        }else{
            hashMap = netstatParser2(process.getInputStream());
        }
        process.waitFor();
        long t2 = System.currentTimeMillis();
        System.out.println(hashMap);
        long t3 = System.currentTimeMillis();

        System.out.println("parsing time : "+(t2-t1)+" ms");
        System.out.println("print time : "+(t3-t2)+" ms");
        System.out.println("total time : "+(t3-t1)+" ms");
        // StandardCharsets.UTF_8.name() > JDK 7
        return hashMap;
    }

    public static HashMap<Integer,Long> netstatParser1(InputStream inputStream){
        HashMap<Integer,Long> hashMap = new HashMap<>();
        String testString =
                        "udp        0      0 0.0.0.0:132             0.0.0.0:*                           2932/socat\n" +
                        "udp        0      0 0.0.0.0:134             0.0.0.0:*                           2949/socat";
//        StringBuilder sb = new StringBuilder();
        StringBuilder port = new StringBuilder();
        StringBuilder pid = new StringBuilder();
        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
            int r;
            while ((r = buffer.read()) != -1) {
                char c = (char) r;
//                sb.append((char) r);
                if (c == ':'){
//                    sb.append(c);
                    while ((r = buffer.read()) != ' ') {
                        c = (char) r;
//                        sb.append(c);
                        port.append(c);
                        continue;
                    }
//                    sb.append((char)r);
                    while ((r = buffer.read()) != ':') {
//                        sb.append((char)r);
                    }
//                    sb.append((char) r);
                    while ((r = buffer.read()) != ' ') {
//                        sb.append((char) r);
                    }
//                    sb.append((char)r);
                    while ((r = buffer.read()) == ' ') {
//                        sb.append((char)r);
                    }
                    pid.append((char)r);
//                    sb.append((char)r);
                    while ((r = buffer.read()) != '/') {
                        c = (char) r;
                        pid.append(c);
//                        sb.append(c);
                    }
//                    sb.append((char)r);
                    while ((r = buffer.read()) != '\n' ) {
                        if(r == -1) break;
//                        sb.append((char)r);
                    }
                    hashMap.put(Integer.parseInt(port.toString()),Long.parseLong(pid.toString()));
//                    System.out.println("port : "+port);
//                    System.out.println("pid : "+pid);
//                    System.out.println(sb);
                    port.setLength(0);
                    pid.setLength(0);
//                    sb.setLength(0);
                }
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<Integer,Long> netstatParser2(InputStream inputStream){
        HashMap<Integer,Long> hashMap = new HashMap<>();
        String testString =
                        "udp        0      0 0.0.0.0:132             0.0.0.0:*                           2932/socat\n" +
                        "udp        0      0 0.0.0.0:134             0.0.0.0:*                           2949/socat";
//        StringBuilder sb = new StringBuilder();
        StringBuilder port = new StringBuilder();
        StringBuilder pid = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split("\\s+");
                hashMap.put(Integer.parseInt(words[3].split(":")[1]),Long.parseLong(words[5].split("/")[0]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<Integer, Long> runNetstatCommand2() throws IOException, InterruptedException {
        HashMap<Integer, Long> hashMap;
        ProcessBuilder processBuilder = new ProcessBuilder(new String[]{"/bin/bash", "-c", "sudo netstat -ulpn | grep 'socat' | awk '{print $4\" \"$6}' | cut -d \":\" -f 2 | cut -d \"/\" -f 1"});
        Process process = processBuilder.start();
        long t1 = System.currentTimeMillis();
        hashMap = netstat2Parser(process.getInputStream());
        process.waitFor();
        long t2 = System.currentTimeMillis();
        System.out.println(hashMap);
        long t3 = System.currentTimeMillis();

        System.out.println("parsing time : "+(t2-t1)+" ms");
        System.out.println("print time : "+(t3-t2)+" ms");
        System.out.println("total time : "+(t3-t1)+" ms");
        // StandardCharsets.UTF_8.name() > JDK 7
        return hashMap;
    }

    public static HashMap<Integer,Long> netstat2Parser(InputStream inputStream){
        HashMap<Integer,Long> hashMap = new HashMap<>();
        String testString =
                "132 2932\n" +
                        "134 2949";

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split("\\s+");
                hashMap.put(Integer.parseInt(words[0]),Long.parseLong(words[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static HashMap<Integer, Long> runPsAuxCommand(int debugMode) throws IOException, InterruptedException {
        HashMap<Integer, Long> hashMap;
        ProcessBuilder processBuilder = new ProcessBuilder(new String[]{"/bin/bash", "-c", "ps -aux | grep 'socat' | awk '{split($16,a,\",\");split(a[1],b,\":\");split($17,c,\":\");print $2\" \"b[2]\" \"c[4]}'"});
        Process process = processBuilder.start();
        long t1 = System.currentTimeMillis();

        hashMap = psAuxParser(process.getInputStream(),debugMode);

        process.waitFor();
        long t2 = System.currentTimeMillis();
        if(debugMode == 1) System.out.println(hashMap);
        long t3 = System.currentTimeMillis();

        System.out.println("psAux parsing time : "+(t2-t1)+" ms");
        System.out.println("psAux parsed hashmap print time : "+(t3-t2)+" ms");
        System.out.println("total psAux processing time : "+(t3-t1)+" ms");
        // StandardCharsets.UTF_8.name() > JDK 7
        return hashMap;
    }

    public static HashMap<Integer, Long> psAuxParser(InputStream inputStream,int debugMode){
        HashMap<Integer,Long> hashMap = new HashMap<>();
        String testString =
                                "393242 132 133\n" +
                                "393243 132 133\n" +
                                "394011";

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                if(debugMode == 1) System.out.println("splitting line : "+line+" hashmap : "+hashMap);
                String[] words = line.split("\\s+");
                if(words.length != 3) continue;
                Long pid = Long.parseLong(words[0]);
                int port1 = Integer.parseInt(words[1]);
                int port2 = Integer.parseInt(words[2]);
                if(!hashMap.containsKey(port1)){
                    hashMap.put(port1,pid);
                }else{
                    hashMap.put(port2,pid);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static void ShutdownSocat(String[] args) throws IOException, InterruptedException {
        HashMap<Integer,Long> portPids;

        portPids = runPsAuxCommand(0);

        int mode = Integer.parseInt(args[0]);
        long t1 = System.currentTimeMillis();
        if(mode == 0){
            portPids.forEach((port,pid)->{
                if(!ProcessHandle.of(pid).get().destroyForcibly()) System.out.println("couldn't request killing for ("+port+","+pid+")");;
            });
        }
        long t2 = System.currentTimeMillis();
        System.out.println("total kill time : "+(t2-t1)+"");
    }

    @Deprecated
    private static void BenchmarkingLsofCommand() throws IOException, InterruptedException {
        String s1;
        long t3 = System.currentTimeMillis();
//        for(int i=0; i<100; i++){
            s1 = ExecComand.runCommand2("sudo lsof -t -i:"+132);
            System.out.println("s1 :"+s1+" port : "+132);
            if(s1.length() != 0) System.out.println("process running on port : "+132+" "+s1+" "+s1.length());
//        }
        long t4 = System.currentTimeMillis();

//        System.out.println("sudo lsof -t -i:132 : "+ExecComand.runCommand("sudo lsof -t -i:132")+" => "+(t2-t1)+"ms");
//        System.out.println("sudo fuser 132/udp : "+ExecComand.runCommand("sudo fuser 132/udp")+" => "+(t3-t2)+"ms");

        System.out.println();

        System.out.println("(optimized) sudo lsof -t -i:132 => "+(t4-t3)+"ms");
    }

    @Deprecated
    public static void pidOnPort(int port) throws IOException, InterruptedException {

        String pid = ExecComand.runCommand2("sudo lsof -t -i:"+port);
        pid = pid.substring(0, pid.length()-1);
        System.out.println("process pid : ("+pid+") on port "+port);
        System.out.println("process parent pid : "+ProcessHandle.of(Long.parseLong(pid)).get().parent().get().pid());
    }

    @Deprecated
    public static void SocatRun(String[] args) throws IOException, InterruptedException {
        HashMap<Integer, String> hashmap = new HashMap<>();
// sudo SOCAT_SOCKADDR=192.168.11.131 socat -d -d -T 10 -t 10 UDP4-LISTEN:132,reuseaddr,fork UDP4:192.168.11.130:130,bind=192.168.11.131:133
// sudo java -jar SoCatImpl.jar 192.168.11.131 132 133 192.168.11.130 130 10
        String serverIp = args[0];
        String serverPort1 = args[1];
        String serverPort2 = args[2];
        String termIp = args[3];
        String termPort = args[4];
        int timeout = Integer.parseInt(args[5]);

        String command = CoreSoCatOperation(serverIp, serverPort1, serverPort2, termIp, termPort, timeout);
        //command = "ping google.com";
        System.out.println("=> "+command);
        System.out.println();

        runCommandFromThread(command);
        pidOnPort(Integer.parseInt(args[1]));
        System.out.println();

//        ArrayList<String> arrayList = runCommand(command);
//        System.out.println("list : ");
//        System.out.println(arrayList);

    }

    @Deprecated
    public static String runCommandReturnString(String command) throws IOException, InterruptedException {
        ByteArrayOutputStream result2 = new ByteArrayOutputStream();
        byte[] buffer2 = new byte[1024];
        Process proc = Runtime.getRuntime().exec(command);
        for (int length; (length = proc.getInputStream().read(buffer2)) != -1; ) {
            result2.write(buffer2, 0, length);
        }
        int exitCode = proc.waitFor();

        if(exitCode !=0 ){
            System.out.println("exit code "+exitCode+" on command : "+command);
        }
        // StandardCharsets.UTF_8.name() > JDK 7
        return result2.toString("UTF-8");
    }

    @Deprecated
    public static void ScanBenchmark(String[] args) throws IOException, InterruptedException {
        int start = Integer.parseInt(args[0]);
        int end = Integer.parseInt(args[1]);
        int debugMode = Integer.parseInt(args[2]);
        long t1 = System.currentTimeMillis();
        for(int i=start ; i<end; i++){
            String s1 = runCommand("lsof -t -i:"+i,debugMode).get(0);
            if(s1.length() > 0){
                if(debugMode == 1) System.out.println("port : "+i+" pid : "+s1);
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println("total time : "+(t2-t1)+" ms");
    }

    public static boolean portOpenSocket(int port, int debug){
        try (Socket socket = new Socket("localhost",port)) {
            socket.close();
            return true;
        } catch (Exception e) {
            if(debug == 1) System.out.println("(Socket) port "+port+" occupied");
            if(debug == 1) e.printStackTrace();
            return false;
        }
    }

    public static boolean portOpenServerSocket(int port, int debug){
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            serverSocket.close();
            return true;
        } catch (IOException e) {
            if(debug == 1) System.out.println("(ServerSocket) port "+port+" occupied");
            if(debug == 1) e.printStackTrace();
            return false;
        }
    }

    public static boolean portOpenDatagramSocket(String serverIp, int port, int debug) {
        try {
            DatagramSocket socket = new DatagramSocket(port, InetAddress.getByName(serverIp));
            socket.close();
            return true;
        } catch (SocketException e) {
            if(debug == 1) System.out.println("(DatagramSocket) port "+port+" occupied");
            if(debug == 1) e.printStackTrace();
            return false;
        } catch (UnknownHostException e) {
            if(debug == 1) System.out.println("(DatagramSocket) port "+port+" occupied");
            if(debug == 1) e.printStackTrace();
            return false;
        }
    }

    public static void portScanBenchmark(String[] args){
        int mode = Integer.parseInt(args[0]);
        String serverIp = args[1];
        int startPort = Integer.parseInt(args[2]);
        int endPort   = Integer.parseInt(args[3]);
        int debugMode = Integer.parseInt(args[4]);
        int occupiedPortCount = 0;
        long t1 = System.currentTimeMillis();
        switch (mode){
            case 1 :
                for(int i=startPort ; i<=endPort ; i++){
                    if(!portOpenSocket(i,debugMode)) occupiedPortCount++;
                }
                break;
            case 2 :
                for(int i=startPort ; i<=endPort ; i++){
                    if(!portOpenServerSocket(i,debugMode)) occupiedPortCount++;
                }
                break;
            case 3 :
                for(int i=startPort ; i<=endPort ; i++){
                    if(!portOpenDatagramSocket(serverIp,i,debugMode)) occupiedPortCount++;
                }
                break;
        }

        long t2 = System.currentTimeMillis();
        System.out.println("=> "+occupiedPortCount+" occupied out of "+(endPort-startPort+1)+" ports");
        System.out.println("** total time spent : "+(t2-t1)+" ms");

    }

    @Deprecated
    public static void SocatThreadBenchmark(String args[]){
        //sudo java -jar SoCatImpl.jar 192.168.11.131 132 133 192.168.11.130 130 10 10 1
        int threadCount = Integer.parseInt(args[6]);
        int debugMode = Integer.parseInt(args[7]);

        SoCatThread[] soCatThreads = new SoCatThread[threadCount];

        for(int i=0; i< soCatThreads.length; i++){
            if(i!=0){
                args[1] = (Integer.parseInt(args[1])+2)+"";
                args[2] = (Integer.parseInt(args[2])+2)+"";
                args[4] = (Integer.parseInt(args[4])+1)+"";
            }

            soCatThreads[i] = new SoCatThread(args,debugMode);
        }
        System.out.println("starting SocatThreads ..");
        System.out.println();

        for(int i=0; i< soCatThreads.length ; i++){
            soCatThreads[i].start();
        }
    }

    public static void runSocatStartCommand(String command, boolean isWait, int debugMode){
        Process process = null;
        try {
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("sh", "-c", command);
            process = processBuilder.start();

            if(isWait){
                int exitCode = process.waitFor();
                if(exitCode != 0){
                    System.out.println("couldn't start properly, exited with exitCode : "
                            +exitCode+" for command: "+command);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void RawSocatBenchmark(String[] args){
        //sudo java -jar SoCatImpl.jar 192.168.11.131 132 133 192.168.11.130 130 10 2 1 true
        int socatCount = Integer.parseInt(args[6]);
        int debugMode = Integer.parseInt(args[7]);
        boolean isWait = Boolean.parseBoolean(args[8]);

        String[] soCatCommands = new String[socatCount];

        for(int i=0; i< soCatCommands.length; i++){
            if(i!=0){
                args[1] = (Integer.parseInt(args[1])+2)+"";
                args[2] = (Integer.parseInt(args[2])+2)+"";
                args[4] = (Integer.parseInt(args[4])+1)+"";
            }

            String serverIp = args[0];
            String serverPort1 = args[1];
            String serverPort2 = args[2];
            String termIp = args[3];
            String termPort = args[4];
            int timeout = Integer.parseInt(args[5]);

            soCatCommands[i] = CoreSoCatOperation(serverIp, serverPort1, serverPort2, termIp, termPort, timeout);
        }
        System.out.println("starting Socat Processes ..");
        System.out.println();
        long t1 = System.currentTimeMillis();
        for(int i=0; i< soCatCommands.length ; i++){
            if(debugMode == 1) System.out.println("executing => "+soCatCommands[i]);
            runSocatStartCommand(soCatCommands[i], isWait, debugMode);
        }
        long t2 = System.currentTimeMillis();
        System.out.println("total start time : "+(t2-t1)+" ms");
    }

    public static void main(String[] args) throws IOException, InterruptedException {

//        SoCatThread soCatThread = new SoCatThread(args, 1);
//        soCatThread.start();
//        SocatRun(args);
//        SocatThreadBenchmark(args);
//        ScanBenchmark(args);
//        portScanBenchmark(args);
        //netstatSocatScan(Integer.parseInt(args[0]));
//        runCommand("ping google.com",1);
//        runNetstatCommand(Integer.parseInt(args[0]));
        //runNetstatCommand2();
//        runPsAuxCommand(Integer.parseInt(args[0]));
//        String testString =
//                "393242 132 133\n" +
//                        "393243 132 133\n" +
//                        "394011";
//        System.out.println(psAuxParser(new ByteArrayInputStream(testString.getBytes())));
//        String testString =
//                        "udp        0      0 0.0.0.0:132             0.0.0.0:*                           2932/socat\n" +
//                        "udp        0      0 0.0.0.0:134             0.0.0.0:*                           2949/socat";
//        netstatParser1(new ByteArrayInputStream(testString.getBytes()));
//        netstatParser2(new ByteArrayInputStream(testString.getBytes()));
//        RawSocatBenchmark(args);
        ShutdownSocat(args);

    }
}
