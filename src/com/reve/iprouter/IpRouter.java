package com.reve.iprouter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class IpRouter {

    /**
     * iptables additional rules should be as tightly coupled as possible, so that it would have minimal
     * impact on network and routing will be faster
     * @param orgIp incoming ip to server
     * @param orgPort incoming port to server
     * @param terIp forwarding ip
     * @param terPort forwarding port
     * @param serverIp server ip
     * @param serverPort server port
     * @param serverInterface network interface
     * @param ruleMode 1 - append mode, 0 - delete mode
     * @param mirrorMode 1 - full duplex forwarding , 0 - one way forwarding from src to dst
     */
    private static ArrayList<String> CoreIpTuplesOperation(String serverIp, String serverPort,
                                                           String orgIp, String orgPort, String terIp, String terPort,
                                                           String serverInterface, int ruleMode, int mirrorMode){

        ArrayList<String> arrayList = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();

        String rule = ruleMode == 1 ? "append" : "delete";

        int limit = 0;
        if(mirrorMode == 1) limit = 2;
        else{limit = 1;}

        for(int i = 0; i < limit ; i++) {
            // adding mirror rules by swapping src and dst ip/port
            if(i == 1){
                orgIp = orgIp + terIp;
                terIp = orgIp.substring(0, orgIp.length() - terIp.length());
                orgIp = orgIp.substring(terIp.length());

                orgPort = orgPort + terPort;
                terPort = orgPort.substring(0, orgPort.length() - terPort.length());
                orgPort = orgPort.substring(terPort.length());
            }


            // DNAT - forward to other destination
            stringBuilder.append("sudo iptables --table nat --").append(rule).append(" PREROUTING --protocol udp")
                    .append(" --in-interface ").append(serverInterface)
                    .append(" --source ").append(orgIp).append(" --sport ").append(orgPort)
                    .append(" --destination ").append(serverIp).append(" --dport ").append(serverPort)
                    .append(" --jump DNAT ")
                    .append("--to-destination ").append(terIp).append(":").append(terPort);
            arrayList.add(stringBuilder.toString());
            stringBuilder.setLength(0);

            // SNAT - while forwarding, chinging it's source
            stringBuilder.append("sudo iptables --table nat --").append(rule).append(" POSTROUTING --protocol udp")
                    .append(" --out-interface ").append(serverInterface)
                    .append(" --source ").append(orgIp).append(" --sport ").append(orgPort)
                    .append(" --destination ").append(terIp).append(" --dport ").append(terPort)
                    .append(" --jump SNAT ")
                    .append("--to-source ").append(serverIp).append(":").append(serverPort);
            arrayList.add(stringBuilder.toString());
            stringBuilder.setLength(0);

            // destroying any previous connection on server port so it will force the next packets from/to this port
            // to look into iptables
            stringBuilder.append("sudo conntrack -p udp -D")
                    .append(" --src ").append(orgIp).append(" --sport ").append(orgPort)
                    .append(" --dst ").append(serverIp).append(" --dport ").append(serverPort);
            arrayList.add(stringBuilder.toString());
            stringBuilder.setLength(0);
        }

        return arrayList;
    }

    public static ArrayList<String> AppendDuplexIpForwarding(String serverIp, String serverPort,
                                                             String orgIp, String orgPort, String terIp, String terPort,
                                                             String serverInterface){
        return CoreIpTuplesOperation(serverIp, serverPort, orgIp, orgPort, terIp, terPort,
                serverInterface, 1, 1);
    }

    public static ArrayList<String> DeleteDuplexIpForwarding(String serverIp, String serverPort,
                                                             String orgIp, String orgPort, String terIp, String terPort,
                                                             String serverInterface){
        return CoreIpTuplesOperation(orgIp, orgPort, terIp, terPort, serverIp, serverPort,
                serverInterface, 0, 1);
    }



    public static void main(String[] args){
        // ex command : java -jar IpRouter.jar 192.168.11.131 131 192.168.11.129 129 192.168.11.130 130 ens33 0 10 2
        try {

            System.out.println("process started ..");

            String serverIp         = args[0];
            String serverPort       = args[1];
            String orgIp            = args[2];
            String orgPort          = args[3];
            String terIp            = args[4];
            String terPort          = args[5];
            String serverInterface  = args[6];
            // 0 - delete , 1 - append
            int ruleMode            = Integer.parseInt(args[7]);
            // number of sequential port aquisation
            int numberOfRules       = Integer.parseInt(args[8]);
            // 0 - exec + not print , 1 - exec + print , 2 - not exec + print
            int debugMode           = Integer.parseInt(args[9]);

            ArrayList<String>[] rules = new ArrayList[numberOfRules];
            long t1 = System.currentTimeMillis();
            for(int i = 0 ; i < numberOfRules ; i++){
                rules[i] = CoreIpTuplesOperation(
                        serverIp, (Integer.parseInt( serverPort)+i)+"", orgIp, orgPort, terIp, terPort, serverInterface, ruleMode, 1
                );
                for(int j = 0; j < rules[i].size(); j++){
                    if(debugMode == 1 || debugMode == 2){
                        System.out.println("=> "+rules[i].get(j));
                    }
                    if(debugMode != 2){
                        Runtime.getRuntime().exec(rules[i].get(j));
                    }
                }
            }
            long t2 = System.currentTimeMillis();
            System.out.println("total time needed for execution : "+(t2-t1)+" ms");

            System.out.println("process ended ..");


        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
