package com.reve.iprouter;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

public class FujiCache {

    private ConcurrentHashMap<Object,Object> coreConcurrentHashMap;
    private int maxSize;
    private int expiryTime;

    class TimeToKey
    {
        private Long timestamp;
        private Object key;

        public TimeToKey(Long timestamp, Object key) {
            this.timestamp = timestamp;
            this.key = key;
        }

        public Long getTimestamp() {
            return timestamp;
        }

        public Object getKey() {
            return key;
        }
    }

    private Queue<TimeToKey> timeToKeyQueue = new LinkedList<>();

    //Thread class as a member inner class or Non-static Inner Class of the usage class
    class ExpiryElementCleaner extends Thread
    {
        boolean running ;

        public ExpiryElementCleaner() {
        }

        @Override
        public void run()
        {
            running = true;
            while(running){
                if(coreConcurrentHashMap.size() != 0){
                    long currentTime = System.currentTimeMillis();
                    long diffTime = 0;
                    try {
                        while((diffTime = currentTime - timeToKeyQueue.peek().getTimestamp()) >= expiryTime){
                            System.out.println("time expired, removing ..");
                            coreConcurrentHashMap.remove(timeToKeyQueue.remove().getKey());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                    if(expiryTime - diffTime > 0){
                        try {
                            System.out.println("going to sleep for : "+(expiryTime - diffTime)+" ms");
                            Thread.sleep(expiryTime - diffTime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        public void shutDown(){
            running = false;
        }
    }
    ExpiryElementCleaner expiryElementCleaner = new ExpiryElementCleaner();

    /**
     * @param size highest number of elements to hold
     * @param time expiry time in seconds
     */
    public FujiCache(int size, int time) {
        maxSize = size;
        expiryTime = time;

        coreConcurrentHashMap = new ConcurrentHashMap<>(size);
        expiryElementCleaner.start();
    }

    public void put(Object key, Object value) throws InterruptedException {

        // size restriction in while enqueue
        while(coreConcurrentHashMap.size() >= maxSize){
            System.out.println("size exceed, removing ..");
            coreConcurrentHashMap.remove(timeToKeyQueue.remove().getKey());
        }

        coreConcurrentHashMap.put(key, value);
        timeToKeyQueue.add(new TimeToKey(System.currentTimeMillis(),key));
    }

    public Object get(Object key){;
        return coreConcurrentHashMap.get(key);
    }

    @Override
    public String toString() {
        return "FujiCache{" +
                "coreConcurrentHashMap=" + coreConcurrentHashMap +
                ", timeToKeyQueue(peek)=" + timeToKeyQueue.peek() +
                '}';
    }

    public void shutDownThread(){
        expiryElementCleaner.shutDown();
    }

    public static void main(String[] args) throws InterruptedException {
        FujiCache fujiCache = new FujiCache(5,2000);
        fujiCache.put(5,"hi");
        System.out.println(fujiCache);
        Thread.sleep(2005);
        fujiCache.put(7,"hello");
        System.out.println(fujiCache);
        Thread.sleep(500);
        fujiCache.put(9,"yup");
        System.out.println(fujiCache);

        fujiCache.shutDownThread();
    }
}
