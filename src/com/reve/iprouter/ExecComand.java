package com.reve.iprouter;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class ExecComand {

    public static ByteArrayOutputStream result = new ByteArrayOutputStream();
    public static byte[] buffer = new byte[1024];

    public static ArrayList<String> runCommand(String command) throws IOException, InterruptedException {
        Process proc = Runtime.getRuntime().exec(command);
        // Read the output

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(proc.getInputStream()));
        ArrayList<String> arrays = new ArrayList<>();
        String line = "";
        while((line = reader.readLine()) != null) {
            //System.out.print(line + "\n");
            //System.out.println(line);
            arrays.add(line);
            line = "";
        }

        proc.waitFor();
        return arrays;
    }

    public static String runCommand2(String command) throws IOException {
        result.reset();
        Arrays.fill(buffer, (byte)0);
        Process proc = Runtime.getRuntime().exec(command);
        for (int length; (length = proc.getInputStream().read(buffer)) != -1; ) {
            result.write(buffer, 0, length);
        }
        // StandardCharsets.UTF_8.name() > JDK 7
        return result.toString("UTF-8");
    }

    public static void refreshCommand2() throws IOException {
        result.reset();
        Arrays.fill(buffer, (byte)0);
    }

    public static ArrayList<String> getPidsFromPort(String port) throws IOException, InterruptedException {
        return runCommand("sudo lsof -t -i:"+port);
    }

    public static void killPort(String port) throws IOException, InterruptedException {
        ArrayList<String> pids = getPidsFromPort(port);
        for(int i=0;i< pids.size();i++){
            System.out.println("killing PID of : "+pids.get(i));
            killPid(pids.get(i));
        }
    }

    public static void killPid(String pid) throws IOException, InterruptedException {
        runCommand("sudo kill "+pid);
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("process started ..");
        String port = "132";
        runCommand("echo killing process from port : "+port);
        killPort(port);
        //killPid();
    }
}
