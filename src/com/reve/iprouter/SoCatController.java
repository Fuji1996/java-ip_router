package com.reve.iprouter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.util.HashMap;

/**
 * We can scan for available port from 0 to 65,500 ports very fast,
 * find PID on any ports or range of ports very fast,
 * start hundreds or thousands(tested with upto 30k) of socat processes,
 * monitor whether a call is running through socat process within thousands of running socat processes efficiently,
 * and shut down all socat services or range of socat services efficiently
 * with the help of this SoCatController class - @Fuji:15/11/22
 */
public class SoCatController {
    /**
     * Its by default a bi-directional connection so no mirror rule addition is needed.
     * it will listen on server ip port1 and forward to term Ip:port using port2 and vice-versa.
     * ruleMode is also not needed as it just needs deletation of PID's running on corrsponding port to
     * destroy corresponding socat rules
     * @param serverIp server ip
     * @param serverPort1 server port 1
     * @param serverPort2 server port 2
     * @param termIp routing ip
     * @param termPort routing port
     * @param timeout timeout
     * @return command
     */
    static String CoreSoCatOperation(String serverIp, String serverPort1, String serverPort2,
                                     String termIp, String termPort, int timeout){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SOCAT_SOCKADDR=").append(serverIp)
                .append(" socat -T ").append(timeout).append(" -t ").append(timeout)
                .append(" UDP4-LISTEN:").append(serverPort1)
                .append(",reuseaddr,fork UDP4:").append(termIp).append(":").append(termPort).append(",")
                .append("bind=").append(serverIp).append(":").append(serverPort2).append("&");
        return stringBuilder.toString();
    }

    /**
     * @param serverIp
     * @param port
     * @param debug whether to print log
     * @return whether the port is available to create datagram socket or not
     */
    public static boolean IsPortOpenDatagramSocket(String serverIp, int port, int debug) {
        try {
            DatagramSocket socket = new DatagramSocket(port, InetAddress.getByName(serverIp));
            socket.close();
            return true;
        } catch (SocketException e) {
            if(debug == 1){
                System.out.println("(DatagramSocket) port "+port+" occupied");
                e.printStackTrace();
            }
            return false;
        } catch (UnknownHostException e) {
            if(debug == 1){
                System.out.println("(DatagramSocket) port "+port+" occupied");
                e.printStackTrace();
            }
            return false;
        }
    }

    /**
     * @param args
     * args[0] - serverIp
     * args[1] - start port for scanning
     * args[2] - end port for scanning
     * args[3] - debug mode : 0/1
     * @return number of occupied port
     */
    public static void PortRangeScanDatagramSocket(String[] args){
        String serverIp = args[0];
        int startPort = Integer.parseInt(args[1]);
        int endPort   = Integer.parseInt(args[2]);
        int debugMode = Integer.parseInt(args[3]);
        int occupiedPortCount = 0;
        long t1 = System.currentTimeMillis();
        for(int i=startPort ; i<=endPort ; i++){
            if(!IsPortOpenDatagramSocket(serverIp,i,debugMode)) occupiedPortCount++;
        }
        long t2 = System.currentTimeMillis();
        System.out.println("=> "+occupiedPortCount+" occupied out of "+(endPort-startPort+1)+" ports");
        System.out.println("** total time spent on scanning : "+(t2-t1)+" ms");
    }

    /**
     * @param args
     * args[0] - serverIp
     * args[1] - serverPort1
     * args[2] - serverPort2
     * args[3] - terminationIp
     * args[4] - terminationPort
     * args[5] - timeout
     * args[6] - number of socat processes I want to start (tested with 30k)
     * args[7] - debugMode : 0/1
     * args[8] - whether it should wait for finishing every command run of socat start
     * it will create args[6] num of socat process and increment port number for every next socat processes
     */
    public static void RunSocatMany(String[] args){
        int socatCount = Integer.parseInt(args[6]);
        int debugMode = Integer.parseInt(args[7]);
        boolean isWait = Boolean.parseBoolean(args[8]);

        String[] soCatCommands = new String[socatCount];

        for(int i=0; i< soCatCommands.length; i++){
            if(i!=0){
                args[1] = (Integer.parseInt(args[1])+2)+"";
                args[2] = (Integer.parseInt(args[2])+2)+"";
                args[4] = (Integer.parseInt(args[4])+1)+"";
            }

            String serverIp = args[0];
            String serverPort1 = args[1];
            String serverPort2 = args[2];
            String termIp = args[3];
            String termPort = args[4];
            int timeout = Integer.parseInt(args[5]);

            soCatCommands[i] = CoreSoCatOperation(serverIp, serverPort1, serverPort2, termIp, termPort, timeout);
        }
        System.out.println("starting Socat Processes ..");
        System.out.println();
        long t1 = System.currentTimeMillis();
        for(int i=0; i< soCatCommands.length ; i++){
            if(debugMode == 1) System.out.println("executing => "+soCatCommands[i]);
            RunSocatStartCommand(soCatCommands[i], isWait, debugMode);
        }
        long t2 = System.currentTimeMillis();
        System.out.println("total start time : "+(t2-t1)+" ms");
    }

    /**
     * run command for single socat process
     * @param command the comand for socat run
     * @param isWait whether it should wait till socat command run is completed
     * @param debugMode debug: : 0/1
     * I decided to choose "ps -aux" command as it was the fastest in test scenario.
     */
    public static void RunSocatStartCommand(String command, boolean isWait, int debugMode){
        Process process = null;
        try {
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("sh", "-c", command);
            process = processBuilder.start();

            if(isWait){
                int exitCode = process.waitFor();
                if(exitCode != 0 && debugMode == 1){
                    System.out.println("couldn't start properly, exited with exitCode : "
                            +exitCode+" for command: "+command);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * @param debugMode whether to print the generated (PORT,PID) hashmap from all running socat processes
     * @return hashmap(PORT,PID) of live socat processes
     */
    public static HashMap<Integer, Long> GetPidPortHashmap(int debugMode) throws IOException, InterruptedException {
        HashMap<Integer, Long> hashMap;
        ProcessBuilder processBuilder = new ProcessBuilder(new String[]{"/bin/bash", "-c", "ps -aux | grep 'socat' | awk '{split($16,a,\",\");split(a[1],b,\":\");split($17,c,\":\");print $2\" \"b[2]\" \"c[4]}'"});
        Process process = processBuilder.start();
        long t1 = System.currentTimeMillis();

        hashMap = ParsePsAuxAndGenerateHashmap(process.getInputStream());

        process.waitFor();
        long t2 = System.currentTimeMillis();
        if(debugMode == 1) System.out.println(hashMap);
        long t3 = System.currentTimeMillis();

        System.out.println("psAux parsing time : "+(t2-t1)+" ms");
        System.out.println("psAux parsed hashmap print time : "+(t3-t2)+" ms");
        System.out.println("total psAux processing time : "+(t3-t1)+" ms");
        // StandardCharsets.UTF_8.name() > JDK 7
        return hashMap;
    }

    /**
     * parsing every row of the output from "ps -aux" command and generate hashmap
     * @param inputStream inputstream of the command output
     * @return Hashmap(PORT,PID) of live socat processes
     */
    public static HashMap<Integer, Long> ParsePsAuxAndGenerateHashmap(InputStream inputStream){
        HashMap<Integer,Long> hashMap = new HashMap<>();
        String exampleString =
                "393242 132 133\n" +
                "393243 132 133\n" +
                "394011";

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split("\\s+");
                if(words.length != 3) continue;
                Long pid = Long.parseLong(words[0]);
                int port1 = Integer.parseInt(words[1]);
                int port2 = Integer.parseInt(words[2]);
                if(!hashMap.containsKey(port1)){
                    hashMap.put(port1,pid);
                }else{
                    hashMap.put(port2,pid);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    /**
     * kill a PID
     * @param pid
     * @param debugMode logging the pid info's
     * @return whether successfully killed the pid or not
     */
    public static boolean KillSocat(long pid, int debugMode){
        if(debugMode == 1) System.out.println("destroying PID : "+pid);
        if(!ProcessHandle.of(pid).get().destroyForcibly()) {
            if(debugMode == 1) System.out.println("couldn't request killing for pid : "+pid+")");
            return false;
        }else{
            return true;
        }
    }

    /**
     * shutting down all socat processes or
     * shutting down socat processes in certain port range
     * @param args
     * args[0] - debugMode : 0/1
     * args[1] - processingMode :
     *              0 - shutdown all socat processes
     *              1 - shutdown socat processes in certain port range
     * if(processingMode == 1) only then args[2] and args[3] will be available
     * args[2] - start port
     * args[3] - end port
     *
     * note : can be optimized more by designing the command to return only pid and kill them in the parsing time for
     * pocessing mode 0. Same goes for processing mode 1, just need to pe check the port range.
     */
    public static void ShutdownSocat(String[] args) throws IOException, InterruptedException {
        HashMap<Integer,Long> portPids;
        int debugMode = Integer.parseInt(args[0]);
        int startPort=0, endPort=0;

        portPids = GetPidPortHashmap(debugMode);

        int processMode = Integer.parseInt(args[1]);
        if(processMode == 1){
            startPort = Integer.parseInt(args[2]);
            endPort = Integer.parseInt(args[3]);
        }

        long t1 = System.currentTimeMillis();
        if(processMode == 0){
            portPids.forEach((port,pid)->{
                try {
                    KillSocat(pid,debugMode);
                } catch (Exception e) {
                    if(debugMode == 1){
                        System.out.println("couldn't destroy -> (port:"+port+", pid:"+pid+")");
                        e.printStackTrace();
                    }
                }
            });
        }else if(processMode == 1){
            for(int i = startPort; i <= endPort ; i++){
                try {
                    KillSocat(portPids.get(i),debugMode);
                } catch (Exception e) {
                    if(debugMode == 1){
                        System.out.println("couldn't destroy pid on -> port : "+i);
                        e.printStackTrace();
                    }
                }
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println("total kill time : "+(t2-t1)+" ms");
    }

    public static void main(String[] args) throws IOException, InterruptedException {
//        System.out.println("hello");
//        RunSocatMany(args);
//        ShutdownSocat(args);
//        GetPidPortHashmap(Integer.parseInt(args[0]));
        PortRangeScanDatagramSocket(args);

    }
}
